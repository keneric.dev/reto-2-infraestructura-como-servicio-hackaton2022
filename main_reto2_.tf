# Especificación de proveedor cloud y versión
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.0"
    }
  }
}

# Configuración de proveedor Microsoft Azure Cloud
provider "azurerm" {
  features {}
}

# Creación de resource group
resource "azurerm_resource_group" "RG-PROD-01" {
  name     = "RG-PROD-01"
  location = "East US 2"
  tags = {
    "CREATEDBY" = "Macintosh Enjoyers",
    "DPT" = "VENTAS",
    "AMBIENTE" = "PRD"
  }
}

# Creación del VNET
resource "azurerm_virtual_network" "VNET-PROD" {
  name                = "VNET-PROD"
  location            = azurerm_resource_group.RG-PROD-01.location
  resource_group_name = azurerm_resource_group.RG-PROD-01.name
  address_space       = ["10.150.0.0/16"]

  tags = {
    "CREATEDBY" = "Macintosh Enjoyers",
    "DPT" = "VENTAS",
    "AMBIENTE" = "PRD"
  }
}

# Creación del Subnet
resource "azurerm_subnet" "Subnet01" {
  name                 = "Subnet01"
  resource_group_name  = azurerm_resource_group.RG-PROD-01.name
  virtual_network_name = azurerm_virtual_network.VNET-PROD.name
  address_prefixes     = ["10.150.0.0/24"]
}

# Creación del network security group para el VNET
resource "azurerm_network_security_group" "VNET-SG-PROD" {
  name                = "VNET-SG-PROD"
  location            = azurerm_resource_group.RG-PROD-01.location
  resource_group_name = azurerm_resource_group.RG-PROD-01.name

  tags = {
    "CREATEDBY" = "Macintosh Enjoyers",
    "DPT" = "VENTAS",
    "AMBIENTE" = "PRD"
  }
}

# Creación de interfaz de red para máquina virtual Windows
resource "azurerm_network_interface" "SRV-PROD-01-NIC-AZ" {
  name                = "SRV-PROD-01-NIC-AZ"
  location            = azurerm_resource_group.RG-PROD-01.location
  resource_group_name = azurerm_resource_group.RG-PROD-01.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.Subnet01.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.PIP-SRV-PROD-01-AZ.id
  }

  tags = {
    "CREATEDBY" = "Macintosh Enjoyers",
    "DPT" = "VENTAS",
    "AMBIENTE" = "PRD"
  }
}

# Creación de interfaz de red para máquina virtual Linux
resource "azurerm_network_interface" "SRV-PROD-02-NIC-AZ" {
  name                = "SRV-PROD-02-NIC-AZ"
  location            = azurerm_resource_group.RG-PROD-01.location
  resource_group_name = azurerm_resource_group.RG-PROD-01.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.Subnet01.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.PIP-SRV-PROD-02-AZ.id
  }

  tags = {
    "CREATEDBY" = "Macintosh Enjoyers",
    "DPT" = "VENTAS",
    "AMBIENTE" = "PRD"
  }
}

# Creación de máquina virtual de Windows
resource "azurerm_windows_virtual_machine" "SRV-PROD-01-AZ" {
  name                = "SRV-PROD-01-AZ"
  resource_group_name = azurerm_resource_group.RG-PROD-01.name
  location            = azurerm_resource_group.RG-PROD-01.location
  size                = "Standard_B2s"
  admin_username      = "joel"
  admin_password      = "Contra123!"
  network_interface_ids = [
    azurerm_network_interface.SRV-PROD-01-NIC-AZ.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
    disk_size_gb = "128"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2019-Datacenter"
    version   = "latest"
  }

  tags = {
    "CREATEDBY" = "Macintosh Enjoyers",
    "DPT" = "VENTAS",
    "AMBIENTE" = "PRD"
  }
}

# Creación de IP pública para máquina virtual Windows
resource "azurerm_public_ip" "PIP-SRV-PROD-01-AZ" {
  name                = "PIP-SRV-PROD-01-AZ"
  resource_group_name = azurerm_resource_group.RG-PROD-01.name
  location            = azurerm_resource_group.RG-PROD-01.location
  allocation_method   = "Static"

  tags = {
    "CREATEDBY" = "Macintosh Enjoyers",
    "DPT" = "VENTAS",
    "AMBIENTE" = "PRD"
  }
}

# Creación de máquina virtual Linux
resource "azurerm_linux_virtual_machine" "SRV-PROD-02-AZ" {
  name                = "SRV-PROD-02-AZ"
  resource_group_name = azurerm_resource_group.RG-PROD-01.name
  location            = azurerm_resource_group.RG-PROD-01.location
  size                = "Standard_B2s"
  admin_username      = "joel"
  admin_password      = "Contra123!"
  disable_password_authentication = false
  network_interface_ids = [
    azurerm_network_interface.SRV-PROD-02-NIC-AZ.id
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
    disk_size_gb = "60"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
}

# Creación de IP pública para máquina virtual Linux
resource "azurerm_public_ip" "PIP-SRV-PROD-02-AZ" {
  name                = "PIP-SRV-PROD-02-AZ"
  resource_group_name = azurerm_resource_group.RG-PROD-01.name
  location            = azurerm_resource_group.RG-PROD-01.location
  allocation_method   = "Static"

  tags = {
    "CREATEDBY" = "Macintosh Enjoyers",
    "DPT" = "VENTAS",
    "AMBIENTE" = "PRD"
  }
}